﻿using DotSpatial.Positioning;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DotSpatialCuba
{
    /* Testing program for accuracy and timing of different algos for distance and baering
     * 
     * Conclusion:
     * 1. best for accuracy => DistanceTo(..) => Normal algo not the fast one
     * 2. 
     */
    class Program
    {
        public bool PrintError = false;
        public Position p1;
        public List<Position> sini;
        public List<Position> sana;
        public List<Distance> dist;
        public List<Azimuth> azimuths;
        //public double MaxDistanceNM = Distance.FromMeters(100.0).ToNauticalMiles().Value;
        public double MaxDistanceNM = 600;

        static void Main(string[] args)
        {
            (new Program(new Position((Latitude)1.02453, (Longitude)4.21554))).ComputePreicision(2468);
            Console.ReadKey();
            //(new Program(new Position((Latitude)41.02453, (Longitude)4.21554))).ComputePreicision();
            //Console.ReadKey();
            //(new Program(new Position((Latitude)1.02453, (Longitude)44.21554))).ComputePreicision();
            //Console.ReadKey();
            //(new Program(new Position((Latitude)41.02453, (Longitude)44.21554))).ComputePreicision();
            //Console.ReadKey();
        }
        public Program(Position p)
        {
            // what to put
            p1 = p;
        }
        public void ComputePreicision(int length = 1000)
        {
            Stopwatch t = new Stopwatch();
            sini = new List<Position>(length);
            sana = new List<Position>(length);
            dist = new List<Distance>(length);
            azimuths = new List<Azimuth>(length);

            List<string> result;

            #region create position
            for (int i = 0; i < length; i++)
            {
                Azimuth a = new Azimuth(720.0 * i / length).Normalize();
                Distance d = Distance.FromNauticalMiles((i * MaxDistanceNM / length) + (0.1 / 1852)).ToMeters();
                Distance o = Distance.FromNauticalMiles(0.5 * i).ToMeters();
                sini.Add(p1.TranslateTo(a, o).Normalize());
                sana.Add(p1.TranslateTo(a, o + d).Normalize());
                dist.Add(d.ToMeters());
                azimuths.Add(a);
            }
            #endregion

            #region Show data set
            Console.WriteLine($"Starting Point = {sana[0]}");
            var ShowInfo = new Action<int, string>((i, s) =>
            {
                Console.WriteLine($"{s,8} Point = {sana[i]} => Distance: {ToUnit(dist[i].ToMeters().Value,'m')}  => Angle {ToUnit(azimuths[i].DecimalDegrees, '°')}");
            });
            ShowInfo(0, "Nearest");
            ShowInfo(1, "Next");
            ShowInfo(length/2, "Middle");
            ShowInfo(length - 1, "Farthest");
            Console.WriteLine("******************************");
            #endregion 
            
            #region Distance - normal algo
            t.Start();
            {
                List<Distance> calc = new List<Distance>(length);

                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Distance d = a.DistanceTo(b);
                    calc.Add(d.ToMeters());
                }
                t.Stop();
                result = Compare(dist, calc, DiffDistance, ShowDistance, 'm', 5);
            }
            ShowResult("Normal", t.Elapsed, result);
            #endregion

            #region Distance - Fast algo
            t.Reset();
            t.Start();
            {
                List<Distance> calc = new List<Distance>(length);
                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Distance d = a.DistanceTo(b, true);
                    calc.Add(d.ToMeters());
                }
                t.Stop();
                result  = Compare(dist, calc, DiffDistance, ShowDistance, 'm');
            }
            ShowResult("Fast", t.Elapsed, result);
            #endregion

            #region Distance - Haversine algo
            t.Reset();
            t.Start();
            {
                List<Distance> calc = new List<Distance>(length);
                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Distance d = a.HaversineDistance(b);
                    calc.Add(d.ToMeters());
                }
                t.Stop();
                result = Compare(dist, calc, DiffDistance, ShowDistance, 'm', 5);
            }
            ShowResult("Haversine", t.Elapsed, result);
            #endregion

            #region Calculate Radius
            //t.Reset();
            //t.Start();
            //{
            //    List<Distance> calc = new List<Distance>(length);
            //    for (int i = 0; i < length; i++)
            //    {
            //        Position a = sini[i];
            //        Position b = sana[i];
            //        a.CompareRadius(b);
            //    }
            //    t.Stop();
            //}
            #endregion

            #region Distance - Rhumb algo
            t.Reset();
            t.Start();
            {
                List<Distance> calc = new List<Distance>(length);
                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Distance d = a.RhumbDistance(b);
                    calc.Add(d.ToMeters());
                }
                t.Stop();
                result = Compare(dist, calc, DiffDistance, ShowDistance, 'm');
            }
            ShowResult("Rhumb", t.Elapsed, result);
            #endregion
            
            #region Angle
            t.Reset();
            t.Start();
            {
                List<Azimuth> calc = new List<Azimuth>(length);
                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Azimuth az = a.BearingTo(b);
                    calc.Add(az);
                }
                t.Stop();
                result = Compare(azimuths, calc, DiffAzimuth, ShowAzimuth, '°');
            }
            ShowResult("Azimuth", t.Elapsed, result);
            #endregion

            #region Angle - Rhumb Angle
            t.Reset();
            t.Start();
            {
                List<Azimuth> calc = new List<Azimuth>(length);
                for (int i = 0; i < length; i++)
                {
                    Position a = sini[i];
                    Position b = sana[i];
                    Azimuth az = a.RhumbBearingTo(b);
                    calc.Add(az);
                }
                t.Stop();
                result = Compare(azimuths, calc, DiffAzimuth, ShowAzimuth, '°');
            }
            ShowResult("RhumbA", t.Elapsed, result);
            #endregion

        }
        public double DiffDistance(Distance a, Distance b) => (a - b).ToMeters().Value;
        public string ShowDistance(Distance a) => $"{a.ToMeters().Value,8:N2}";
        public double DiffAzimuth(Azimuth a, Azimuth b) => (a - b).DecimalDegrees;
        public string ShowAzimuth(Azimuth a) => $"{a.DecimalDegrees,6:N2}";
        public void ShowResult(string jenis, TimeSpan elapsed, List<string> results)
        {
            Console.WriteLine($"{jenis,10} Algo elapsed time: {elapsed.Seconds,3:N0}s {elapsed.TotalMilliseconds,7:N3}ms");
            foreach (var result in results)
                Console.WriteLine($"\t=> {result}");
            Console.WriteLine("******************************");
        }
        public string ToUnit(double precision, char u, bool dec = true )
        {
            if (precision == 0) return $"  0 {u}";

            string unit = "", prefix = "Mk mµ";
            int[] pow = new int[] { 6, 3, 0, -3, -6 };
            for (int i = 0; i < pow.Length; i++)
            {
                double p = Math.Pow(10, pow[i]);
                double pp = precision / p;
                if ((1.0<=pp) && (pp < 1000))
                {
                    string val = dec ? $"{pp,6:N2}" : $"{pp,3:N0}";
                    unit = $"{val}{prefix[i]}{u}";
                    break;
                }
            }
            return unit;
        }
        public List<string> Compare<T>(List<T> ori, List<T> cal, Func<T, T, double> diff, Func<T, string> show, char u, int prec = 6)
        {
            List<string> res = new List<string>(prec);
            double precision;
            for (int i = prec+1; i > 0; i--)
            {
                string pres, error = "", firstError = "";
                precision = Math.Pow(10, i-prec);

                (int m, int n, double min, double d, double max, int err) = Compare(ori, cal, diff, show, precision);

                string unit = ToUnit(precision, u, false);
                pres = $"Precision {unit,6}: {m,5}/{n,-5}";
                if (n != 0) error = $" => min: {ToUnit(min,u)}, avg: {ToUnit(d,u)}, max: {ToUnit(max, u)}";
                if (err != -1) firstError = $" => Point: {err,4}";
                res.Add($"{pres}{error}{firstError}");
                // until find error
                if (n != 0) break;
            }
            return res;
        }
        public (int match, int notmatch, double min, double avg, double max, int i)
            Compare<T>(List<T> ori, List<T> cal, Func<T, T, double> diff, Func<T, string> show, double error, string format = "")
        {
            string s = String.Empty;
            double avg = 0, min = Double.MaxValue, max = Double.MinValue;
            int match = 0, notmatch = 0, err = -1;
            int length = Math.Min(ori.Count, cal.Count);

            // make sure same length???
            if (ori.Count != cal.Count) notmatch = Math.Abs(ori.Count - cal.Count);
            // Compare
            for (int i = length - 1; i >= 0; i--)
            {
                double d = diff(ori[i], cal[i]);

                if (d < error)
                {
                    match++;
                }
                else
                {
                    s += $"{i},";
                    err = i;
                    notmatch++;
                    avg += d;
                    min = d < min ? d : min;
                    max = max < d ? d : max;
                    if (PrintError) Console.WriteLine($"Point: {i,4} ** (Ori,Cal): ({show(ori[i])} , {show(cal[i])}) ** Diff: {d,14:e4}");
                }
            }
            if (PrintError && (s.Length != 0)) Console.WriteLine($"Error: {s}");
            min = notmatch == 0 ? Double.NaN : min;
            max = notmatch == 0 ? Double.NaN : max;
            return (match, notmatch, min, avg/notmatch, max, err);
        }
    }

    public static class MyExtension
    {
        /* Based on BAMCIS GeoCoordinate */
        public static double Radius = Ellipsoid.Wgs1984.EquatorialRadius.ToMeters().Value;
        public static double DegreeToRadian(double d) => d * Math.PI / 180.0;
        public static double RadianToDegree(double d) => d * 180.0 / Math.PI;
        public static Distance HaversineDistance(this Position s, Position d) => s.HaversineDistance(d, Ellipsoid.Wgs1984);
        public static Distance HaversineDistance(this Position s, Position d, Ellipsoid ellipsoid)
        {
            // calculate radian
            double lat1 = DegreeToRadian(s.Latitude.DecimalDegrees);
            double lat2 = DegreeToRadian(d.Latitude.DecimalDegrees);
            double deltaLatitude = DegreeToRadian(d.Latitude.DecimalDegrees - s.Latitude.DecimalDegrees);
            double deltaLongitude = DegreeToRadian(d.Longitude.DecimalDegrees - s.Longitude.DecimalDegrees);
            double A = Math.Pow(Math.Sin(deltaLatitude / 2), 2) +
                Math.Cos(DegreeToRadian(s.Latitude.DecimalDegrees)) * Math.Cos(DegreeToRadian(d.Latitude.DecimalDegrees)) *
                Math.Pow(Math.Sin(deltaLongitude / 2), 2);
            double c = 2 * Math.Asin(Math.Sqrt(A));

            // Calculate average radius
            // https://en.wikipedia.org/wiki/Earth_radius#Global_average_radii
            double a = ellipsoid.EquatorialRadius.ToMeters().Value;
            double b = ellipsoid.PolarRadius.ToMeters().Value;
            double e = ellipsoid.Eccentricity;
            double r = (2 * a + b) / 3.0;
            // or this?
            // https://en.wikipedia.org/wiki/Ellipse#Polar_form_relative_to_center
            r = b / Math.Sqrt(1.0 - Math.Pow(e * Math.Cos((lat2 - lat1) / 2.0), 2));

            // calculate distance / length
            double distance = c * r;

            return new Distance(distance, DistanceUnit.Meters);
        }
        public static Distance RhumbDistance(this Position s, Position d)
        {
            double lat1 = DegreeToRadian(s.Latitude.DecimalDegrees);
            double lat2 = DegreeToRadian(d.Latitude.DecimalDegrees);
            double deltaLatitude = DegreeToRadian(d.Latitude.DecimalDegrees - s.Latitude.DecimalDegrees);
            double deltaLongitude = DegreeToRadian(Math.Abs(d.Longitude.DecimalDegrees - s.Longitude.DecimalDegrees));

            double deltaPhi = Math.Log(Math.Tan(lat2 / 2 + Math.PI / 4) / Math.Tan(lat1 / 2 + Math.PI / 4));
            double Q = Math.Cos(lat1);

            if (deltaPhi != 0)
            {
                Q = deltaLatitude / deltaPhi;  // E-W line gives DeltaPhi=0
            }

            // if DeltaLongitude over 180° take shorter rhumb across 180° meridian
            if (deltaLongitude > Math.PI)
            {
                deltaLongitude = 2 * Math.PI - deltaLongitude;
            }

            double distance = Math.Sqrt(Math.Pow(deltaLatitude, 2) + Math.Pow(Q, 2) * Math.Pow(deltaLongitude, 2)) * Radius;

            return new Distance(distance, DistanceUnit.Meters);
        }
        public static Azimuth RhumbBearingTo(this Position s, Position d)
        {
            double lat1 = DegreeToRadian(s.Latitude.DecimalDegrees);
            double lat2 = DegreeToRadian(d.Latitude.DecimalDegrees); ;
            double deltaLongitude = DegreeToRadian(d.Longitude.DecimalDegrees - s.Longitude.DecimalDegrees);

            double X = Math.Log(Math.Tan(lat2 / 2 + Math.PI / 4) / Math.Tan(lat1 / 2 + Math.PI / 4));

            if (Math.Abs(deltaLongitude) > Math.PI)
            {
                deltaLongitude = (deltaLongitude > 0) ? -(2 * Math.PI - deltaLongitude) : (2 * Math.PI + deltaLongitude);
            }

            double azimuth = Math.Atan2(deltaLongitude, X);

            azimuth = (RadianToDegree(azimuth) + 360) % 360;

            return new Azimuth(azimuth);
        }
        public static void CompareRadius(this Position s, Position d)
        {
            var ellipsoid = Ellipsoid.Wgs1984;
            double lat1 = s.Latitude.ToRadians().Value;
            double lat2 = d.Latitude.ToRadians().Value;
            double long1 = s.Longitude.ToRadians().Value;
            double long2 = d.Longitude.ToRadians().Value;

            double dlat = Math.Abs(lat2 - lat1);
            double dlong = Math.Abs(long2 - long1);

            double l = (lat1 + lat2) * 0.5;

            double a = ellipsoid.EquatorialRadius.ToKilometers().Value;
            double b = ellipsoid.PolarRadius.ToKilometers().Value;

            double e = Math.Sqrt(1 - (b * b) / (a * a));    // eccentricity

            double r1 = (a * (1 - (e * e))) / Math.Pow((1 - (e * e) * (Math.Sin(l) * Math.Sin(l))), 3 * 0.5);
            double r2 = a / Math.Sqrt(1 - (e * e) * (Math.Sin(l) * Math.Sin(l)));

            double ravg = 1000.0 * ((r1 * (dlat / (dlat + dlong))) + (r2 * (dlong / (dlat + dlong))));

            b = ellipsoid.PolarRadius.ToMeters().Value;
            e = ellipsoid.Eccentricity;
            r1 = b / Math.Sqrt(1 - Math.Pow(e * Math.Cos(lat1), 2));
            r2 = b / Math.Sqrt(1 - Math.Pow(e * Math.Cos(lat2), 2));
            double r = (r1 + r2) / 2.0;

            Console.WriteLine($"Radius ref: {Radius,12:N2} <> simple: {r,12:N2} <> Calculated {ravg,12:N2}");
        }
    }
}
